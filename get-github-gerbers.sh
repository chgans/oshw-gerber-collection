# TODO: variation on ascending and descending order? asc vs desc

extensions="gbl gbs gbo gbp gko gm1 g1 gp1 gpb gpt gtp gtl gts gto gbr"

# For each extension fetch the 100 allowed result pages of repository that contains files with that extension
resultdir=github-gerber-queries
#for ext in $extensions;
#do
# echo "Checking for extension $ext ..."
# for page in $(seq 1 100);
# do
#  outdir=$resultdir/$ext
#  mkdir -p $outdir
#  doc=$outdir/$ext-$page.html
#  # Skip existing ones, allow to update extension list and rerun script
#  [ ! -e $doc ] && {
#    echo "Fetching page $page for extension $ext ..."
#    # Get result page as HTML
#    curl "https://github.com/search?p=$page&o=asc&q=extension%3A$ext+g01&s=indexed&type=Code" -o $doc
#	# Cope with rate limit
#	sleep 10;
#	# invalidate list of repository
#	rm -f repos.txt
#	}
# done
#done

# Extract all unique repositories name from their URLs, format is "user/repo"
[ ! -e repos.txt ] && {
  echo "Collecting repo list ..."
  sed -ne 's,^.*<a href="\(/[^/=]*/[^/=]*\)">.*$,\1,gp' $resultdir/*/*.html | sort | uniq > repos.txt
  echo "Found $(wc -l repos.txt | cut -d' ' -f1) repositories"
}

set -x
# Clone them all
for repo in $(cat repos.txt);
do
   dir=repos$repo
   # Skip existing (and successfull) clones
   [ ! -e $dir.DONE ] && {
     echo "cloning $repo into $dir..."
     # Remove repo if it has previously failed
     rm -Rf $dir
	 # clone repo and mark clone as DONE
	 mkdir -p $dir
     git clone --depth 1 https://github.com$repo.git $dir && touch $dir.DONE
   }
done

find repos/ \( -name '*.[gG]??' -o -name '*.[gG]?' \) -a -not -name '.git*' -a -not -name '*.[gG][iI][fF]' > gerber-files.txt
#   gerbers=$(find extensions w/ path) or grep -r 'M02\*'
#   recursive copy of gerbers
#   delete clone
cat gerber-files.txt | while read file;
do
	d=$(dirname "$file");
	dir=$(echo $d|sed 's,^repos/\(.*\)$,\1,g');
	echo $dir;
done

cat gerber-files.txt | while read file; do d=$(dirname "$file"); dir=$(echo $d|sed 's,^repos/\(.*\)$,\1,g'); echo $dir; mkdir -p samples/$dir; echo cp $(echo -n $file|sed -e 's/ /\\ /g') samples/$dir; done