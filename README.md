This repository contains, as of the 29th of August 2016, ca. 36k gerber files collected from github.

The goal of this project is to simply collect gerber files that will be used
to design, code and validate LibreEDA Gerber Viewer (currently in the making
at https://gitlab.com/chgans/le-gerber-viewer).

LibreEDA Gerber Viewer provides a stand-alone gerber parser, that is able to
calculate statistics.

This repository:
 - hosts 38450 might-be-gerber files (ca. 36k files are indeed gerber files)
 - weight 3.1GB (average gerber file size is roughly 100KB)
 - contains 160 GS (GS=million of statements, statement is a G/D/M or
   extended gerber code)

The parser provided with LibreEDA Gerber Viewer can parse the whole repository
in 40 seconds and yields the following code statistics:

  Code | Occurence |  % Total  
:------|----------:|----------:
 <any> | 159632178 | 100.0000% 
 D01   | 111771830 |  70.0184% 
 D02   |  28728286 |  17.9966% 
 D03   |   5999617 |   3.7584% 
 G36   |   4460721 |   2.7944% 
 G37   |   4460554 |   2.7943% 
 Dnn   |   1335582 |   0.8367% 
 G54   |    921751 |   0.5774% 
 AD    |    766558 |   0.4802% 
 G01   |    416837 |   0.2611% 
 G03   |    288444 |   0.1807% 
 G04   |    143400 |   0.0898% 
 G75   |     85007 |   0.0533% 
 G74   |     43018 |   0.0269% 
 M02   |     35705 |   0.0224% 
 MO    |     35593 |   0.0223% 
 FS    |     34235 |   0.0214% 
 AM    |     30753 |   0.0193% 
 G70   |     22238 |   0.0139% 
 G90   |     14358 |   0.0090% 
 LP    |     13221 |   0.0083% 
 G02   |      7423 |   0.0047% 
 LN    |      4837 |   0.0030% 
 G71   |      3906 |   0.0024% 
 IP    |      2032 |   0.0013% 
 IN    |      1498 |   0.0009% 
 OF    |      1154 |   0.0007% 
 SF    |      1112 |   0.0007% 
 TF    |       709 |   0.0004% 
 AS    |       119 |   0.0001% 
 SR    |        88 |   0.0001% 
 TA    |        53 |   0.0000% 
 MI    |        22 |   0.0000% 
 M00   |        18 |   0.0000% 
 IR    |        12 |   0.0000% 
 G55   |         0 |   0.0000% 
 G91   |         0 |   0.0000% 
 M01   |         0 |   0.0000% 
 TD    |         0 |   0.0000% 
 